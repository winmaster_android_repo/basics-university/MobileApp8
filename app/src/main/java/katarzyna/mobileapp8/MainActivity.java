package katarzyna.mobileapp8;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{

    EditText editTextAuthor;
    EditText editTextTitle;
    EditText editTextDescript;
    TextView textViewProcess;
    TextView textViewDurationTime;
    Button buttonErase;
    Button buttonAdd;
    Button buttonStopRec;
    Button buttonRecord;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextAuthor = (EditText) findViewById(R.id.editTextAuthor);
        editTextTitle=(EditText) findViewById(R.id.editTextTitle);
        editTextDescript= (EditText) findViewById(R.id.editTextDescript);
        textViewProcess=(TextView) findViewById(R.id.textViewProcess);
        textViewDurationTime=(TextView) findViewById(R.id.textViewDurationTime);
        buttonErase= (Button) findViewById(R.id.buttonErase);
        buttonAdd=(Button) findViewById(R.id.buttonAdd);
        buttonStopRec=(Button) findViewById(R.id.buttonStopRec);
        buttonRecord=(Button) findViewById(R.id.buttonRecord);
    }

    public boolean onCreateOptionsMenu(Menu second_menu)
    {
        super.onCreateOptionsMenu(second_menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, second_menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.listOfRecords:  //przejdz do listy nagran
            {
                Intent intent =new Intent(this, ListOfRecords.class);
                startActivity(intent);
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void eraseData(View view)
    {
        editTextAuthor.setText("");
        editTextDescript.setText("");
        editTextTitle.setText("");
    }
}
